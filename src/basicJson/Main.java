package basicJson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public class Main {

    //This method converts an Object into JSON
    public static String carsToJSON(Cars cars) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(cars);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    //This method converts JSON to an object
    public static Cars JSONToCars(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Cars cars = null;

        try {
            cars = mapper.readValue(s, Cars.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return cars;
    }

    //Main starts the HTTP server
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(8500), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(Main::handleRequest);
        server.start();
    }

    private static void handleRequest(HttpExchange exchange) throws IOException {

        Cars testOne = new Cars();
        testOne.setMake("Ford");
        testOne.setModel("F-150");
        testOne.setYear(2010);
        testOne.setMillage(56.000);
        testOne.setAvailable(false);

        Cars testTwo = new Cars();
        testTwo.setMake("Ferrari");
        testTwo.setModel("Spider");
        testTwo.setYear(2020);
        testTwo.setMillage(10.000);
        testTwo.setAvailable(true);

        //Calls on carsToJSON to convert an Object into JSON
        String json = Main.carsToJSON(testOne);
        String jsonTwo = Main.carsToJSON(testTwo);

        //Calls on JSONToCars to convert JSON into an Object
        //Outputs the Object that has been converted
        Cars testThree = Main.JSONToCars(json);
        System.out.println(testThree);
        Cars testFour = Main.JSONToCars(jsonTwo);
        System.out.println(testFour);

        //Saves both objects in toHttp variable
        //Uses "\n" to output in separate lines
        String toHttp = jsonTwo + "\n" + json;

        //Outputs JSON when HTTP server is opened on the web browser
        exchange.sendResponseHeaders(200, toHttp.getBytes().length);//response code and length
        OutputStream os = exchange.getResponseBody();
        os.write(toHttp.getBytes());
        os.close();
    }

}

