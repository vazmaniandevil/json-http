package basicJson;

public class Cars {

    private String Make;
    private String Model;
    private Integer Year;
    private Double Millage;
    private Boolean Available;

    public String getMake() {
        return Make;
    }

    public void setMake(String make) {
        Make = make;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public Integer getYear() {
        return Year;
    }

    public void setYear(Integer year) {
        Year = year;
    }

    public Double getMillage() {
        return Millage;
    }

    public void setMillage(Double millage) {
        Millage = millage;
    }

    public Boolean getAvailable() {
        return Available;
    }

    public void setAvailable(Boolean available) {
        Available = available;
    }

    public String toString() {
        return "Make: " + Make + " Model: " + Model + " Year:" + Year + " Millage:" + Millage + " Available:" + Available;
    }
}
